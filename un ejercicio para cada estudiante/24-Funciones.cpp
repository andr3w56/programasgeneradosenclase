#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>
#include <ctime>
#include <sstream>
#include <windows.h>
using namespace std;
/*1. Escriba la función que recibe como parámetro 3 números reales de 
doble precisión y devuelve el promedio de estos números.*/
double promedio(double n1, double n2, double n3){
  std::cout << "Ejercicio 1:" << std::endl;  
  return (n1+n2+n3)/3;
}
/*
2.Escriba la función que recibe como parámetro 3 números reales de doble precisión y devuelve el promedio de estos números. Además, en un parámetro pasado por referencia devuelve la sumatoria de los mismos.
*/
double promedio(double n1, double n2, double n3, double& suma){
  std::cout << "\nEjercicio 2: " << std::endl;
  //suma es un parametro por referencia 
  //n1,n2,n3 son parametros por valor 
  suma=(n1+n2+n3);
  return suma/3;
}

/*3. Escriba la función que determina si un número pasado como parámetro es par o impar.*/
void Multiplode2(int mult2){
  cout << "\n\nEjercicio 3:";
  if (mult2 % 2 == 0){
    cout << "\n" << mult2 << " Es par";
  }
  else{
    cout << "\n" << mult2 << " Es impar";
  }
}

/*4. Escriba la función que determina si un número pasado como parámetro es múltiplo de 5*/
void Multiplode5(int mult){
  cout << "\n\nEjercicio 4:";
  if (mult % 5 == 0){
    cout << "\n" << mult << " Es multiplo de 5";
  }
  else{
    cout << "\n" << mult << " No es multiplo de 5" << endl;
  }
}
/*5. Escriba la función que recibe como parámetro 1 entero y devuelve el cubo del mismo.*/
int Cubo(int n){
  n = pow(n, 2);
  return n;
}
/*6. Una función recibe 2 enteros como parámetros, el primero es el número base, 
mientras que el segundo indica el exponente al que se va elevar el número base. 
Por ejemplo, si el primer número es 2 y el segúndo número es 5, el proceso 
a realizar sería 2 x 2 x 2 x 2 x 2 = 32 (en otras palabras 2 elevado a la quinta potencia).*/
void Elevar(double num1, double num2){
  cout << "\nEjercicio 6:\n";
  double elevado = pow(num1, num2);
  cout << "El numero " << num1 << " elevado al " << num2 << " es " << elevado;
}
/*7. Diseñe la función que determina la cantidad de cifras que tiene un número entero - este entero pasa como parámetro. Por ejemplo:*/
template<typename T>
size_t countDigits(T n)
{
    string tmp;

    tmp = to_string(n);
    return tmp.size();
}
void Cifras(int num1, int num2){
  cout << "\n\nEjercicio 7:\n";
  cout << "El numero de cifras en " << num1 << " es igual a: " << countDigits(num1) << endl;
  cout << "El numero de cifras en " << num2 << " es igual a: " << countDigits(num2) << endl;
}
/*8. Escriba la función que recibe como parámetro 1 entero positivo e imprime su equivalente en Binario.*/
string Binario(int &n){
  string bin;
  while (n!= 0){
    bin += (n % 2 == 0 ? "0" : "1" );
    n/= 2;
  }
  return bin;
}

/*9. Diseñe la función que recibe como parámetro 1 entero positivo que indica una tabla de multiplicar. 
La función debe imprimir la tabla de multiplicar desde el 1 al 15. Por ejemplo:
5 x 1 = 5

5 x 2 = 10

5 x 3 = 15

...

...

5 x 15 = 75 */

void TablaMult(int n){
  cout << "\n\nEjercicio 9:";
  for (int i = 0; i <= 15; i++){
    cout << "\n" << n << " x " << i << " = " << (n*i) << endl;
  }
}
/*10. Diseñe la función que recibe como parámetro 1 entero positivo que indica una tabla de multiplicar. La función debe imprimir la tabla de multiplicar desde el 12 al 1. Por ejemplo:

5 x 12 = 60

5 x 11 = 55

5 x 10 = 50

...

...

5 x 1 = 5*/
int TablaDescendente(int n){
  cout << "\nEjercicio 10:";
  for (int i = 12; i >= 1; i--){
    cout << "\n" <<  n << " x " << i << " = " << (n*i) << endl;
  }
  return n;
}

/*11. Diseñe la función que recibe como parámetro un entero N comprendido 1 y 10. 
La función debe imprimir las tablas de múltiplicar desde el 1 a N. Por ejemplo, si N=3, se imprimirá:

Tabla del 1

1 x 1 = 1

1 x 2 = 2

...

1 x 10 = 10



Tabla del 2

2 x 1 = 2

2 x 2 = 4

...

2 x 10 = 20



Tabla del 3

3 x 1 = 3

3 x 2 = 3

...

3 x 10 = 30*/
void Tablas(int final){
  cout << "\nEjercicio 11:";
  if (final >= 1 and final <= 10){
    for (int i = 1; i <= final; i++)
      for (int k = 1; k <= final; k++){
        cout << "\n" << i << " X " << k << " = " << (i * k);
    }
  }
}
/*12. Diseñe la función que calcula la ecuación cuadrática, usando las fórmulas siguientes:

x1 = (-b+ raizCuadrada(b*b - 4*a*c))/(2*a)

x2 = (-b- raizCuadrada(b*b - 4*a*c))/(2*a)

Los valores a,b,c pasan como parámetros de la función.

x1 - x2 son parámetros por referencia que devuelven los resultados de la ecuación.

La función devuelve 1 si es una ecuación válida, y 0 si la ecuación no es válida.*/
int funcionCuadratica(double a, double b, double c, double &x1, double &x2){
  cout << "\n\nEjercicio 12:";
  if (a == 0) //error division para 0
    return 0;

  double d = (b * b)-(4*a*c);
  if (d < 0)
    return 0; //error discriminante negativo

  x1 = (-b + sqrt(d))/(2 * a);
  x2 = (-b-sqrt(d))/(2 * a);
    return 1;
  if ((b * b)-4*a*c){
    return 1;
  }else{
    return 0;
  }  
}
/*13. Diseñe la función que devuelve el resultado de la siguiente serie:

1 - 2 + 3 - 4 + 5 - 6 + 7 - N

N es un parámetro que indica el límite de la serie*/
void Recursion(int n){
  cout << "\n\nEjercicio 13:";
  int r = 0;
  for(int i = 1 ; i <=  n; i ++){
    if(i%2 == 0){
        r -= i;
    }else{
      r += i;
    }
  }
  cout << r << endl;
}
/*14. Diseñe la función que calcula el resultado de la siguiente serie:

2^1 + 3^2 + 4^3 + 5^4 + ... N^(N-1)

N es un parámetro que indica el límite de la serie

Nota: El simbolo ^ significa: elevado a*/

void SerieExplonente(int n){
  double r = 0;
  for (int i = 2; i <= n; i++){
    r = pow(i,(i-1));
  }
  cout<<"El resultado de la serie es: "<< r << endl;
}
/*15. Diseñe la función que calcula el resultado de la siguiente serie:

2! - 3! + 4! - 5! + 6! - N!

N es un parámetro que indica el límite de la serie

Nota: El símbolo ! significa: factorial*/
void ReFactorial(int n){
	int r; long int f = 1;
	for(int i = 2; i <= n; i++){
		f *= i;
	  if(f%2 == 0){
		  r = (-f) - r;
	  }else{
		  r = f + r;
	  }
  }   
  cout<<"El resultado de la serie es: "<< r << endl;
}
/*16. Una función recibe como parámetro un entero que indica las filas de una figura. 
Adicionalmente, recibe como parámetro un string que indica el relleno de la figura a dibujar. 
Por ejemplo, si filas es 4 y el relleno es '+', la figura sería:

+

++

+++

++++ */
void Filas(string f, int nf){
  cout << "\n\nEjercicio 16:\n";
   for(int i = 1; i <= nf; ++i)
    {
        for(int j = 1; j <= i; ++j)
        {
            cout << f;
        }
        cout << "\n";
    }
}
/*17. Figura en espejo: Una función recibe como parámetro un entero que indica las filas de una figura. Adicionalmente, recibe como parámetro un string que indica el relleno de la figura a dibujar. Por ejemplo, si filas es 4 y el relleno es 'o', la figura sería:

o

oo

ooo

oooo

ooo

oo

o*/
void Espejo(string r,int q){
  cout << "\n\nEjercicio 17:\n";
  for(int i = 1;i <= q; i++){
    for(int o = 1; o <= i; o++)
      cout << r;
      cout << endl;
  }
  for(int a = q; a >= 1; a--){
    for(int y = a; y >= 1; y--)
      cout << r;
      cout << endl;
  }
}
/*18. Una función recibe 3 números enteros. 
La función devuelve verdadero si uno de esos números es la suma de los otros dos.*/
bool Sumadelos3(int &f, int &g, int &h){
  cout << "\n\nEjercicio 18:\n";
  if (f + g == h){
    return true;
  }else if (f + h == g){
    return true;
  }else if (g + h == f){
    return true;
  }else{
    return false;
  }
}
/*19. Una función recibe como parámetro un entero. La función debe imprimir este entero en su equivalente en hexadecimal(base 16).*/
void Hexadecimal(int num){
  //Bucle: repetir proceso

  while (num > 0){
    int res = num % 16;
    num = num / 16;
    if (res==10)
      cout<<"A";
    else if (res==11)
      cout<<"B";
    else if (res==12)
      cout<<"C";
    else if (res==13)
      cout<<"D";
    else if (res==14)
      cout<<"E";
    else if (res==15)
      cout<<"F";
    else
      cout<<res;
    }
}
/*20. Una función recibe como parámetro 3 enteros que indican el año, mes y día del nacimiento de una persona. La función debe devolver la edad de la persona en años, meses y días. Debe investigar como capturar la fecha actual de la computadora.*/
void Edad(int año, int mes, int dia){
  cout << "\n\nEjercicio 20:\n";
  time_t t = time(0);// Obtiene la fecha actual desde el sistema
  tm* now = std::localtime(&t);
  int añoActual = 1900 + now->tm_year;
  int mesActual = now->tm_mon + 1;
  int diaActual = now->tm_mday;
  año = añoActual - año;
  dia = año * 365;
  mes = 365 * año;
  mes /= 30;
  std::cout << "Actualemente tienes: " << año << " años, " << mes << " meses, " << dia << " dias." << std::endl;
}
/*21. Una función recibe como parámetro 3 enteros que indican el año, mes y día de una fecha cualquiera. La función debe devolver los días transcurridos desde esa fecha comparada con la fecha actual de la computadora.*/
void DiasTranscurridos(int año, int mes, int dia){
  cout << "\n\nEjercicio 21:\n";
  time_t t = time(0);// Obtiene la fecha actual desde el sistema
  tm* now = std::localtime(&t);
  int añoActual = 1900 + now->tm_year;
  int mesActual = now->tm_mon + 1;
  int diaActual = now->tm_mday;
  int Dtranscurridos = añoActual - año;
  std::cout << Dtranscurridos * 365;
}
/*22. Escriba la función que recupera la hora actual de la computadora 
y la imprime por pantalla. Luego intente hacer el proceso para que la hora se actualice cada segundo.*/
void Tiempo(){
  cout << "\n\nEjercicio 22:\n";

  bool exit = false;
  int i = 0;
  while(exit == false){
    i++;
    auto t = time(nullptr);
    auto tm = *localtime(&t);

    std::ostringstream oss;
    oss << put_time(&tm, "%d-%m-%Y %H:%M:%S");
    auto str = oss.str();

    cout << str << endl;
    if (GetAsyncKeyState(VK_ESCAPE)){
      exit = true;
    }
  }
}
/*23. Una función recibe como parámetro un entero que indica la edad de una persona en días. La función debe imprimir la edad de la persona en años, meses y días.*/
void Edad(int dias){
  cout << "\n\nEjercicio 23:\n";
  int años, dia, meses;
  std::cout << "La edad en años es: " << dias / 365 << std::endl;
  std::cout << "La edad en dias es: "<< dias << std::endl;
  std::cout << "La edad en meses es: " << dias / 30;
}
/*24. Diseñe la función que recibe como parámetro 2 enteros y los devuelve intercambiados. 
Por ejemplo si num1=45 y num2=50, luego de llamar a la función num1 sería igual a 50 y num2 igual a 45.*/
void swap(int &n1, int &n2){
  cout << "\n\nEjercicio 24:\n";
  int aux = n1;
  n1 = n2;
  n2 = aux;
}

int main(int argc, char *argv[]) {
  double p = promedio(18.5, 20.11, 23.99);
  cout << "Promedio de 3 numeros: " << endl;
  cout << "El promedio es: "<< p << endl;


  double suma; 
  p = promedio(22.4, 35, 30, suma);
  cout <<"La suma de 22.4 + 35 + 30 es: "<< suma;
  cout << ", y el promedio es: "<< p;

  Multiplode2(60);
  Multiplode5(20);
  
  std::cout << "\n\nEjercicio 5: " << std::endl;
  std::cout << "El cubo de 5 es: " << Cubo(5) << std::endl;

  Elevar(6, 2);
  Cifras(25, 900);

  cout << "\n\nEjercicio 8:\n";
  int num = 17;
  cout<<"Decimal: "<< num << endl;
  cout<<"Binario: "<< Binario(num) << endl;

  TablaMult(5);
  TablaDescendente(5);
  Tablas(3);

  double x1;
  double x2;

  int r = funcionCuadratica(1.5, 6.6, 3, x1, x2);
  if (r = 1){
    cout << "\nLa primera solucion es: " << x1;
    cout << "\nLa primera solucion es: " << x2;
  }else if (r == -1){
    cout << "Error al dividir para 0, o discriminante negativo";
  }
  else if (r == 0){
    cout << "Discriminante negativo (soluciones imaginarios).";
  }

  Recursion(8);
  SerieExplonente(7);
  ReFactorial(7);

  Filas("*", 4);
  Espejo("+",5);
  int n1 = 5; int n2 = 10; int n3 = 5;
  if ((Sumadelos3(n1, n2, n3))== true){
    std::cout << "Verdadero" << std::endl;
  }else{
    std::cout << "Falso" << std::endl;
  }
  Hexadecimal(1256);

  Edad(2004, 7, 28);
  DiasTranscurridos(2004, 7, 28);
  Tiempo();
  Edad(6558);

  int a = 45, b = 50;
  swap(a, b);
  std::cout << "a = " << a << std::endl;
  std::cout << "b =" << b << std::endl;
  return 0;
}
