#include <iostream>
#include <math.h>
using namespace std;

/*5. Escriba la función que recibe como parámetro 1 entero 
y devuelve el cubo del mismo.*/
void Numero(double num){
    cout << "Ejercicio 5:\n";
    double elevado = pow(num, 2);
    cout << "El numero " << num << " elevado al cuadrado es " << elevado << endl;
}

/*6. Una función recibe 2 enteros como parámetros, 
el primero es el número base, mientras que el segundo indica el exponente 
al que se va elevar el número base. Por ejemplo, si el primer número es 2 y 
el segúndo número es 5, el proceso a realizar sería 2 x 2 x 2 x 2 x 2 = 32 
(en otras palabras 2 elevado a la quinta potencia).*/
void Numero(double num1, double num2){
    cout << "\nEjercicio 6:\n";
    double elevado = pow(num1, num2);
    cout << "El numero " << num1 << " elevado al " << num2 << " es " << elevado << endl;
}

/*7. Diseñe la función que determina la cantidad de cifras que tiene un número entero - este entero pasa como parámetro. Por ejemplo:

si el número es 12 tiene 2 cifras

si el número es 157 tiene 3 cifras

si el número es 999 tiene 3 cifras

si el número es 15000 tiene 5 cifras. 

si el número es -145 tiene 3 cifras*/
template<typename T>
size_t countDigits(T n)
{
    string tmp;

    tmp = to_string(n);
    return tmp.size();
}
void Cifras(int num1, int num2){
    cout << "\nEjercicio 7: \n";
    cout << "El numero de cifras en " << num1 << " es igual a: " << countDigits(num1) << endl;
    cout << "El numero de cifras en " << num2 << " es igual a: " << countDigits(num2) << endl;
}

/*13. Diseñe la función que devuelve el resultado de la siguiente serie:

1 - 2 + 3 - 4 + 5 - 6 + 7 - N

N es un parámetro que indica el límite de la serie*/
void Recursion(int n){
    cout << "\nEjercicio 13:\n";
    int r = 0;
    for(int i = 1 ; i <=  n; i ++){
        if(i%2 == 0){
            r += i;
        }else{
            r -= i;
        }
    }
    cout << "El resultado de la serie hasta N es: " << r;
}

int main(int argc, char const *argv[]){
    Numero(5);
    Numero(2,5);
    Cifras(20,1009);
    
    Recursion(8); 
    return 0;
}