#include <iostream>
#include <vector>
using namespace std;
void Corazones4(){
    std::cout << R"(Corazones (Devin Llerena):
                     ╔═══════════════╗
                     ║ 4             ║
                     ║ ♥           ♥ ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║ ♥           ♥ ║
                     ║             4 ║
                     ╚═══════════════╝)" << endl;

}
void Pica4(){
    std::cout << R"( Picas (Angel Mora):
                     ╔═══════════════╗ 
                     ║ 4             ║
                     ║ ♠             ║
                     ║   ♠       ♠   ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║   ♠       ♠   ║
                     ║             ♠ ║
                     ║             4 ║
                     ╚═══════════════╝)" << endl; //Carta de Pica creada por Angel :p
}
void Diamante4(){
    std::cout << R"( Diamantes (Andrew Gonzales):      
                     ╔═══════════════╗ 
                     ║ 4             ║
                     ║ ◆             ║
                     ║   ◆       ◆   ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║   ◆       ◆   ║
                     ║             ◆ ║
                     ║             4 ║
                     ╚═══════════════╝)" << endl; // Andrew
}
void Trebol4(){
    std::cout << R"(Treboles (Andrew Gonzales):
                     ╔═══════════════╗ 
                     ║ 4             ║
                     ║ ☘             ║
                     ║   ☘       ☘   ║
                     ║               ║
                     ║               ║
                     ║               ║
                     ║   ☘       ☘   ║
                     ║             ☘ ║
                     ║             4 ║
                     ╚═══════════════╝)" << endl; // Andrew
}
int main(int argc, char const *argv[]){
    Corazones4();
    Pica4();
    Diamante4();
    Trebol4();
    return 0;
}
