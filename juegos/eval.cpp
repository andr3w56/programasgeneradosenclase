#include <iostream>
using namespace std;
string Mayor1(int nota){
    string A = "A", B = "B", C = "C", D = "D", F = "F", N = "NOTA NO VALIDA";
    if(nota >= 1 and nota < 60){
        return F;
    }else if (nota >= 60 and nota < 70){
        return D;
    }else if (nota >= 70 and nota < 80){
        return C;
    }else if (nota >= 80 and nota < 90){
        return B;
    }else if (nota >= 90 and nota <= 100){
        return A;
    }else{
        return N;
    }
}
void EnteroAOctal(int decimalNum) {
   int octalNum = 0, valor = 1;
   int Numero = decimalNum;
   while (decimalNum != 0) {
      octalNum += (decimalNum % 8) * valor;
      decimalNum /= 8;
      valor *= 10;
   }
   cout<<"El entero "<<Numero<<" a octal(base8) es: "<<octalNum<<endl;
}
int main(int argc, char const *argv[]){
    cout << Mayor1(101) << endl; // Cuando es 101
    cout << Mayor1(1) << endl; //Cuando es 1
    EnteroAOctal(8);
    return 0;
}
