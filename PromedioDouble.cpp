#include <iostream>
using namespace std;

double promedio(double n1, double n2, double n3){
    return (n1+n2+n3)/3;
}

double promedio(double n1, double n2, double n3, double &suma){
    suma = (n1 + n2 + n3);
    return suma/3;
}

bool parImpar(int num){
    //devuelve true cuando es par, devuelve false cuando es impar
    /*if(num % 2==0)
        return true; //es par
    else
        return false; //es impar*/
    return num % 2;                
}

int main(int argc, char const *argv[])
{
    cout << "\nPromedio de 3 numeros: ";
    double p = promedio(18.5, 20.11, 23.99);
    cout << "\nEl promedio es: " << p;

    cout << "\nEl promedio de otros 3 es de: ";
    cout << promedio(5, 20.5, 90.2);

    double s; p = promedio(22.4, 35, 30, s);
    cout << "\nLa suma de 22.4 + 35 + 30 es: " << s;
    cout << ", y el promedio es: " << p;

    if (parImpar(16))
        cout << "\Es par";
    else
        cout << "\nEs impar";

    return 0;
}
