#include <iostream>
using namespace std;
/*18. Una función recibe 3 números enteros.
La función devuelve verdadero si uno de esos números es la suma de los otros dos.*/
void Sumadelos3(int f, int g, int h){
  cout << "\n\nEjercicio 24:\n";
  if (f + g == h){
    cout << "Verdadero";
  }
  else if (f + h == g){
    cout << "Verdadero";
  }
  else if (g + h == f){
    cout << "Verdadero";
  }
  else{
    cout << "Falso";
  }
}


int main(int argc, char *argv[]) {
  Sumadelos3(5, 10, 5);

  return 0;
}
