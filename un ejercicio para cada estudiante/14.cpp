#include <iostream>
#include <cmath>
using namespace std;
/*14. Diseñe la función que calcula el resultado de la siguiente serie:

2^1 + 3^2 + 4^3 + 5^4 + ... N^(N-1)

N es un parámetro que indica el límite de la serie

Nota: El simbolo ^ significa: elevado a*/

void SerieExplonente(int n){
    double r = 0;
    for (int i = 2; i <= n; i++){
            r = pow(i,(i-1));
        }
    cout<<"El resultado de la serie es: "<< r << endl;
}
int main(int argc, char const *argv[]){
	SerieExplonente(7);
    return 0;
}
