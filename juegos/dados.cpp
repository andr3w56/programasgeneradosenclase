#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int generaNumAleatorio(){
    int num = 1+rand()%(6);
    return num;
}

void Mostrardado(int num){
    if (num == 1){
        std::cout << " ▓ " << std::endl;
    }
    else if (num == 2){
        std::cout << "▓  ▓" << std::endl;
    }
    else if (num == 3){
        std::cout << "▓  ▓  ▓" << std::endl;
    }
    else if (num == 4){
        std::cout << "▓  ▓" << std::endl;
        std::cout << "▓  ▓" << std::endl;
    }
    else if (num == 5){
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "  ▓  ▓ " << std::endl;
    }
    else if (num == 6){
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "▓  ▓  ▓" << std::endl;
    }
    else if (num == 7){
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "   ▓   " << std::endl;
    }
    else if (num == 8){
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "  ▓  ▓  " << std::endl;
    }
    else if (num == 9){
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "▓  ▓  ▓" << std::endl;
        std::cout << "▓  ▓  ▓" << std::endl;    
    }
}

void ValidaGanador(int dado1, int dado2){
    if(dado1 == dado2){
        std::cout << "FELICIDADES, ERES SUPER GANADOR" << std::endl;
    }else if ((dado1 + dado2) == 7 or (dado1 + dado2) == 11){
        std::cout << "FELICIDADES, ERES GANADOR" << std::endl;
    }else{
        std::cout << "PERDISTE!!" << std::endl;
    }
}

int main(int argc, char const *argv[])
{
    srand(time(NULL));
    std::cout << "Pulsa la tecla <ENTER> para lanzar los dados" << std::endl;
    getchar();
    std::cout << std::endl;
    int dado1 = generaNumAleatorio();
    int dado2 = generaNumAleatorio();
    std::cout << std::endl;
    
    std::cout << "Dado 1 = ";
    std::cout << dado1 << std::endl;
    std::cout << "Dado 2 = ";
    std::cout << dado2 << std::endl;

    std::cout << std::endl;
    ValidaGanador(dado1, dado2);
    return 0;
}
