#include <iostream>
using namespace std;

void swap(int &n1, int &n2){
    int aux = n1;
    n1 = n2;
    n2 = aux;
}

int main(int argc, char const *argv[]){
    int n1 = 10, n2 = 15;
    swap(n1, n2);

    int a = 78, b = 5;
    swap(a, b);

    std::cout << "n1 = " << n1 << std::endl;
    std::cout << "a =" << n2 << std::endl;
    return 0;
}
